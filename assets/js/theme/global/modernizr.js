import $ from 'jquery';
import modernizr from 'modernizr';

function copyImageToBackground() {
    const $this = $(this);
    const classList = $this.attr('class').split(/\s+/);
    const $container = $this.parent();
    const imgUrl = $this.hasClass('lazyload') ? $this.attr('data-src') : $this.attr('src');
    let fitMode;

    if (classList.includes('object-fit--cover')) {
        fitMode = 'cover';
    } else if (classList.includes('object-fit--contain')) {
        fitMode = 'contain';
    }

    if (imgUrl) {
        $container
            .css('backgroundImage', `url('${imgUrl}')`)
            .addClass(`compat-object-fit compat-object-fit--${fitMode}`);
    }
}

export default function () {
    if (!modernizr.objectfit) {
        $('[class*="object-fit--"]').each(copyImageToBackground);
    }
}
