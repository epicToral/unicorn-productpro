import $ from 'jquery';

function matchElementHeights() {
    if ($(window).width() > 769) {
        $('[data-match-height-parent]').each((index, element) => {
            const $elements = $(element).find('[data-match-height]');
            let height = 0;
            $elements.css('min-height', '0px');
            $elements.each((i, el) => {
                height = $(el).height() > height ? $(el).height() : height;
            });
            $elements.css('min-height', `${height}px`);
        });
    } else {
        $('[data-match-height-parent]').each((index, element) => {
            $(element).find('[data-match-height]').css('min-height', 'auto');
        });
    }
}

export default function () {
    matchElementHeights();
    // Reslick on screen size change.
    let resizeTimer = 0;
    // const parent = this;
    $(window).on('resize', () => {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(() => {
            matchElementHeights();
        }, 250);
    });
}
