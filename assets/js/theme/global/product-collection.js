import $ from 'jquery';
import SlickSlider from '../global/slick-slider';

export default function () {
    // Slick slider for mobile.
    const slider = new SlickSlider();
    slider.initSlickSlider($('[data-mobile-slider]'), {
        mobileFirst: true,
        arrows: false,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        lazyLoad: 'ondemand',
        responsive: [
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
        ],
    }, 769);
}
