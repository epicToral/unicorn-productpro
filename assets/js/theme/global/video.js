import $ from 'jquery';
import 'foundation-sites/js/foundation/foundation';
import 'foundation-sites/js/foundation/foundation.dropdown';
import { defaultModal } from './modal';


export default function () {
    const modal = defaultModal();

    $('body').on('click', '[data-video]', event => {
        event.preventDefault();
        if ($(event.currentTarget).data('video-youtube').length) {
            const video = $(event.currentTarget).data('video-youtube');
            const player = `<iframe id="ytplayer" type="text/html" width="640" height="360" src="${video}" frameborder="0"></iframe>`;
            modal.open({ size: 'video' });
            modal.updateContent(`<div class='container'><div class='embed-container'>${player}</div></div>`);
        } else if ($(event.currentTarget).data('video-vimeo').length) {
            const video = $(event.currentTarget).data('video-vimeo');
            const player = `<iframe src="${video}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>`;
            modal.open({ size: 'video' });
            modal.updateContent(`<div class='container'><div class='embed-container'>${player}</div></div>`);
        }
    });
}
