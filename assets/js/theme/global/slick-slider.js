import $ from 'jquery';

export default class SlickSlider {
    // Reapply slick slider when switching over the 'unslick' breakpoint.
    reSlickSlider($slickSlider, settings, unslickBreakpoint) {
        if ($(window).width() > unslickBreakpoint) {
            if ($slickSlider.hasClass('slick-initialized')) {
                $slickSlider.slick('unslick');
            }
            return;
        }

        if (!$slickSlider.hasClass('slick-initialized')) {
            return $slickSlider.slick(settings);
        }
    }

    initSlickSlider($slickSlider, settings, unslickBreakpoint) {
        // Intialise slick slider
        if ($(window).width() < unslickBreakpoint) {
            $slickSlider.slick(settings);
        }

        // Reslick on screen size change.
        let resizeTimer = 0;
        const parent = this;
        $(window).on('resize', () => {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(() => {
                parent.reSlickSlider($slickSlider, settings, unslickBreakpoint);
            }, 250);
        });
    }
}
