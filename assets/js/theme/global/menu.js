import $ from 'jquery';
import collapsibleFactory from '../common/collapsible';
import collapsibleGroupFactory from '../common/collapsible-group';

const PLUGIN_KEY = 'menu';

/*
 * Manage the behaviour of a menu
 * @param {jQuery} $menu
 */
class Menu {
    constructor($menu) {
        this.$menu = $menu;
        this.$body = $('body');
        this.$window = $(window);
        this.$document = $(document);
        this.hasMaxMenuDisplayDepth = this.$body.find('.navPages-list').hasClass('navPages-list-depth-max');

        // Init collapsible
        this.collapsibles = collapsibleFactory('[data-collapsible]', { $context: this.$menu });
        this.collapsibleGroups = collapsibleGroupFactory($menu);

        // Auto-bind
        this.onMenuClick = this.onMenuClick.bind(this);
        this.onDocumentClick = this.onDocumentClick.bind(this);
        this.onWindowScroll = this.onWindowScroll.bind(this);

        // Listen
        this.bindEvents();

        // Sticky header
        this.headerElement = $('header.header');
        this.miniCart = $('#cart-preview-dropdown');
        this.elHeight = 0;
        this.elTop = 0;
        this.dHeight = 0;
        this.wHeight = 0;
        this.wScrollCurrent = 0;
        this.wScrollBefore = 0;
        this.wScrollDiff = 0;
    }

    collapseAll() {
        this.collapsibles.forEach(collapsible => collapsible.close());
        this.collapsibleGroups.forEach(group => group.close());
    }

    collapseNeighbors($neighbors) {
        const $collapsibles = collapsibleFactory('[data-collapsible]', { $context: $neighbors });

        $collapsibles.forEach($collapsible => $collapsible.close());
    }

    bindEvents() {
        this.$menu.on('click', this.onMenuClick);
        this.$body.on('click', this.onDocumentClick);
        this.$window.on('scroll', this.onWindowScroll);
    }

    unbindEvents() {
        this.$menu.off('click', this.onMenuClick);
        this.$body.off('click', this.onDocumentClick);
    }

    onMenuClick(event) {
        event.stopPropagation();

        if (this.hasMaxMenuDisplayDepth) {
            const $neighbors = $(event.target).parent().siblings();

            this.collapseNeighbors($neighbors);
        }
    }

    onDocumentClick() {
        this.collapseAll();
    }

    onWindowScroll() {
        this.elHeight = this.headerElement.outerHeight() + 65;
        this.dHeight = this.$document.height();
        this.wHeight = this.$window.height();
        this.wScrollCurrent = this.$window.scrollTop();
        this.wScrollDiff = this.wScrollBefore - this.wScrollCurrent;
        this.elTop = parseInt(this.headerElement.css('top'), 10) + this.wScrollDiff;

        if (this.$window.scrollTop() > this.elHeight && $('body').hasClass('is-scrolling') === false && this.wScrollDiff < 0) {
            $('body').addClass('is-scrolling');
        } else if (this.$window.scrollTop() <= 0 && $('body').hasClass('is-scrolling') === true) {
            $('body').removeClass('is-scrolling');
        }

        if (this.wScrollCurrent <= 0) {
            // scrolled to the very top; element sticks to the top
            this.headerElement.css('top', 0);
            $('body').removeClass('is-scrolling-up');
            $('body').removeClass('is-scrolling-down');
        } else if (this.wScrollDiff > 0) {
            // scrolled up; element slides in
            this.headerElement.css('top', this.elTop > 0 ? 0 : this.elTop);
            $('body').addClass('is-scrolling-up');
            $('body').removeClass('is-scrolling-down');
        } else if (this.wScrollDiff < 0) {
            // scrolled down
            this.headerElement.css('top', Math.abs(this.elTop) > this.elHeight ? -this.elHeight : this.elTop);
            $('body').addClass('is-scrolling-down');
            $('body').removeClass('is-scrolling-up');
        }
        this.wScrollBefore = this.wScrollCurrent;
    }
}

/*
 * Create a new Menu instance
 * @param {string} [selector]
 * @return {Menu}
 */
export default function menuFactory(selector = `[data-${PLUGIN_KEY}]`) {
    const $menu = $(selector).eq(0);
    const instanceKey = `${PLUGIN_KEY}Instance`;
    const cachedMenu = $menu.data(instanceKey);

    if (cachedMenu instanceof Menu) {
        return cachedMenu;
    }

    const menu = new Menu($menu);

    $menu.data(instanceKey, menu);

    return menu;
}
