import $ from 'jquery';

const jumpSelector = 'jump-to-top';
class Jump {
    constructor() {
        // Elements
        this.$window = $(window);
        this.$document = $(document);
        this.$body = $('body');
        this.$jumper = $('.jump-to-top');
        this.$footer = $('.footer');

        // Event binders
        this.jumpToTop = this.jumpToTop.bind(this);
        this.placeJumpToTop = this.placeJumpToTop.bind(this);

        this.$jumper.on('click', this.jumpToTop);
        this.$window.on('scroll resize', this.placeJumpToTop);

        // Initialize quantities at 0
        this.elHeight = 0;
        this.elTop = 0;
        this.dHeight = 0;
        this.wHeight = 0;
        this.wScrollCurrent = 0;
        this.wScrollBefore = 0;
        this.wScrollDiff = 0;
        this.jumpThresholdDistance = 50;

        this.placeJumpToTop();
    }

    isInViewport(el) {
        const rect = el.getBoundingClientRect();
        return (
            rect.bottom >= 0 &&
            rect.top <= (this.wHeight)
        );
    }

    placeJumpToTop() {
        this.jumperHeight = this.$jumper.outerHeight();
        this.footerHeight = this.$footer.outerHeight();
        this.dHeight = this.$document.height();
        this.wHeight = this.$window.height();
        this.wScrollCurrent = this.$window.scrollTop();
        this.wScrollDiff = this.wScrollBefore - this.wScrollCurrent;
        this.elBottom = parseInt(this.$jumper.css('top'), 10) + this.wScrollDiff;

        if (this.wScrollCurrent <= this.jumpThresholdDistance) {
            // Don't show the jump to top if within a certain threshold of the top of the page
            this.$jumper.removeClass('jump-to-top--visible');
        } else {
            this.$jumper.addClass('jump-to-top--visible');
        }

        if (this.isInViewport(this.$footer[0])) {
            // If this footer is in view, lock the jump button to it's parent element
            this.$jumper.removeClass('jump-to-top--fixed');
        } else {
            // Otherwise, set the position as required
            this.$jumper.addClass('jump-to-top--fixed');
        }

        this.wScrollBefore = this.wScrollCurrent;
    }

    jumpToTop() {
        window.scroll({
            top: 0,
            behavior: 'smooth',
        });
    }
}

export default function jumpFactory(selector = jumpSelector) {
    const $jump = $(`#${selector}`).eq(0);
    const jump = new Jump($jump);
    return jump;
}
