import $ from 'jquery';
import 'foundation-sites/js/foundation/foundation';
import 'foundation-sites/js/foundation/foundation.dropdown';
import utils from '@bigcommerce/stencil-utils';

const loadingClass = 'is-loading';
const $cart = $('[data-cart-preview]');
const $cartDropdown = $('#cart-preview-dropdown');
const $cartLoading = $('<div class="loadingOverlay"></div>');
const $quantityPill = $('#countPill-wrapper');
const $fullCartPage = $('.pages-cart');
const options = {
    template: {
        cartPreview: 'common/cart-preview',
        countPill: 'common/count-pill',
    },
};


function renderCart() {
    $cartDropdown
        .addClass(loadingClass);
    $cartLoading
        .show();
    utils.api.cart.getContent(options, (err, response) => {
        $cartDropdown
            .removeClass(loadingClass)
            .html(response.cartPreview);
        $cartLoading
            .hide();
        $quantityPill.html(response.countPill);
        // eslint-disable-next-line no-use-before-define
        $('[data-remove-item]', $cartDropdown).on('click', cartRemoveItem);
    });
}

function cartRemoveItem(event) {
    const $this = $(event.target).closest('[data-item-id]');
    utils.api.cart.itemRemove($this.data('item-id'), (err, response) => {
        if (response.data.status === 'succeed') {
            if ($fullCartPage.length) {
                return window.location.reload(true);
            }
            renderCart();
        } else {
            // Handle error
        }
    });
}

export const CartPreviewEvents = {
    close: 'closed.fndtn.dropdown',
    open: 'opened.fndtn.dropdown',
};

export default function () {
    $('body').on('cart-quantity-update', (event, quantity) => {
        $('.cart-quantity')
            .text(quantity)
            .toggleClass('countPill--positive', quantity > 0);
        $('.navUser-item--cart .navUser-action').toggleClass('is-full', quantity > 0);
    });

    $cart.on('click', event => {
        // Redirect to full cart page
        //
        // https://developer.mozilla.org/en-US/docs/Browser_detection_using_the_user_agent
        // In summary, we recommend looking for the string 'Mobi' anywhere in the User Agent to detect a mobile device.
        // if (/Mobi/i.test(navigator.userAgent)) {
        // return event.stopPropagation();
        // }
        event.preventDefault();
        renderCart();
    });
}
