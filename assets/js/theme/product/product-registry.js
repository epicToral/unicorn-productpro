import $ from 'jquery';
import ribon from '../common/ribon';

/* eslint-disable */
export default class {
    constructor() {
    	this.registryAdd = false;
        this.registries = {};
		
		if (!window.ribon_data) {
			this.requestTokens(true);
		} else {
			this.registry();
		}
    }
	
	addRegistryItem(fields, attributes) {
        // const fields = $form.serializeArray();
        let valid = true;
        let post = {
            productid: this.product_id,
            qty: this.reg_qty,
            registryid: this.registryid,
            attributes: attributes || {},
            qty_purchased: 0,
            qty_updated: 0,
        };
        if (!attributes) {
            attributes = {};
            for(let i = 0; i < fields.length; i++) {
                const field = fields[i];
                const target = $('input[name="'+field.name+'"]');

                if (target.prop('required') !== false && field.value === '') {
                    valid = false;
                    break;
                }

                if (field.name.indexOf('attribute') > -1) {
                    const attr = field.name.replace(/attribute\[([0-9]*)\]/g, '$1');
                    const value = parseFloat(field.value);
                    attributes[attr] = value;
                }

                if (field.name.indexOf('qty') > -1) {
                    post.qty = parseFloat(field.value);
                }

                if (field.name.indexOf('product_id') > -1) {
                    post.productid = parseFloat(field.value);
                }
            }
        }

        // console.log("valid", valid, attributes, post);
        if (valid) {
            post.attributes = JSON.stringify(attributes);
            post.token = window.data_token;
            const main = this;
            ribon.basicPost(window.ribon_data + '/api/v1/data/registry_products', post).then(data => {
            	window.location.href = "/registry/view/#/" + main.registries[post.registryid].link;
            });
        }
    }

    validateProduct($form) {
        this.registryAdd = false;
        const fields = $form.serializeArray();

        // we have to manually check radio and checkboxes
        // console.log('input', $form[0]);
        let valid = true;
        $('input', $form).each((a,input) => {
            const name = $(input).attr('name');
            if ($(input).attr('required') && $(input).attr('type') === 'radio') {
                // this means that it's required
                if (!$('input[name="'+name+'"]:checked').val()) {
                    valid = false;
                }

            }
        });

        if (valid) {
            this.addRegistryItem(fields);
        } else {
            alert('Please select all required fields');
        }
    }

    registry() {
        // let's check if the customer is present
        // console.log("registry", $('.customer_id').length, $('.customer_id').length);
        const main = this;
        if ($('.customer_id').length > -1) {
            // let's get the registries from the database
            ribon.basicGet(window.ribon_data + '/api/v1/data/registries', { token: window.data_token, customerid: $('.customer_id').html() }).then(registries => {
            	// console.log('registries', registries);
                if (registries.length === 0) {
                    $('.available-registries').hide();
                    $('.no-registries').show();
                } else {
                    $('.available-registries').show();
                    $('.no-registries').hide();
                }
                // let's append these registries to the select
                for(let i = 0; i < registries.length; i++) {
                    const reg = registries[i];
                    main.registries[reg.id] = reg;
                    $('select.registries').append('<option value="'+reg.id+'">'+reg.name+'</option>')
                }
            });
        }
        $('body').on('click', 'a.add-registry-product', event => {
            const target = $(event.currentTarget);
            const reg = target.siblings('select.registries').val();
            const form = $('form[data-cart-item-add]');

            this.registryid = reg;
            this.product_id = $('input[name="product_id"]').val();
            this.reg_qty = form.find('.form-input--incrementTotal').val();

            // let's make sure that there is a registry selected
            if (isNaN(reg)) {
                if ($('select.registries option').length > 1) {
                    alert('Please select a registry');
                } else {
                    // let's just take them to the registry creation page
                    // console.log("options", $('select.registries option').length);
                    window.location.href = "/registry/create-registry";
                }
                return false;
            }

            this.registryAdd = true;
            form.submit();
        });

        // while we're here let's also create the bind event
        $('body').on('click', 'a.add-registry', event => {
            const target = $(event.currentTarget);
            const reg = target.siblings('select.registries').val();
            this.registryid = reg;
            this.product_id = target.attr('data-product-id');
            this.reg_qty = target.parent().parent('.accordion-content').find('.form-input').val();

            // let's make sure that there is a registry selected
            if (isNaN(reg)) {
                if ($('select.registries option').length > 1) {
                    alert('Please select a registry');
                } else {
                    // let's just take them to the registry creation page
                    // console.log("options", $('select.registries option').length);
                    window.location.href = "/registry/create-registry";
                }
                return false;
            }

            // console.log("add to registry", this.registryid, this.product_id);
            const form = target.parent().parent('.accordion-content');
            const container = target.parent().parent('.accordion-content');
            let attributes = {};
            this.registryAdd = true;

            // let's check if all options are selected
            // console.log('options', container);
            let valid = true;
            $('input, select', container).each(function(index,option){
                // console.log('index', index, option);
                let name = $(option).attr('name');
                let required = $(option).attr('required');
                let type = $(option).attr('type');
                if (!name) {
                    name = "";
                }
                let attribute_id = name.replace(/attribute\[(.*)\]/g, '$1');
                let option_id = "";

                if (required && type == 'radio') {
                    // console.log('radio', $('input[name="'+name+'"]:checked').val());
                    if (!$('input[name="'+name+'"]:checked').val()) {
                        valid = false;
                    } else {
                        option_id = $('input[name="'+name+'"]:checked').val();
                    }
                } else if (required) {
                    // this is probably a select
                    // console.log('value', $(option).val());
                    if ($(option).val() == '') {
                        valid = false;
                    } else {
                        option_id = $(option).val();
                    }
                }

                if (attribute_id != "" && option_id != "")
                    attributes[attribute_id] = option_id;
            });

            if (!valid) {
                target.parent().siblings('.error-message').text('Please select valid options');
                target.parent().siblings('.error-message').show();
                return false;
            }            

            this.addRegistryItem([], attributes);

        });
    }

    requestTokens(init) {
    	ribon.basicGet(window.ribon + '/api/store/'+window.store+'/services/tokens').then(tokens => {
    		let exp = 3600;
            let now = Math.round(new Date().getTime()/1000)
            for(let i = 0; i < tokens.length; i++) {
                let token = tokens[i];

                window['ribon_' + token.name] = token.url;
                window[token.name + '_token'] = token.token;

                let next = Math.round(new Date(token.expires).getTime()/1000);

                if ((next - now) < exp) {
                    exp = next - now;
                }
            }

            if (init) {
            	this.registry();
            }

            setTimeout(function() {
                this.requestTokens(false);
            }, exp * 1000);
    	});
    }
}