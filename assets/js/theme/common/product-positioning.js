import $ from 'jquery';
import async from 'async';
import utils from '@bigcommerce/stencil-utils';

export default function sortProducts(context) {
    context.categoryProductsPerPage = 24;
    const storeId = context.themeSettings.calender_store_id;
    const category = $('#current-category-id').val();
    const displayMode = context.themeSettings.product_list_display_mode;
    const template = displayMode === 'grid' ? 'category/product-card' : 'category/product-list-item';
    const listClass = displayMode === 'grid' ? '.productGrid' : '.productList';
    const limit = context.categoryProductsPerPage || 12;
    var page = location.search.split('page=')[1];
    var pageInt = parseInt(page);
    const StoreUrl  = context.themeSettings.toolbox_version;
    const offset = context.categoryProductsPerPage *(pageInt - 1)  || 0;
    // Get product sorting from Merchandising API
    // also pass in the store id, category id, products per page, and offset
    return $.getJSON(`${StoreUrl}app/js/merchandising-stencil`, {
        store_id: storeId,
        category,
        limit,
        offset,
    })
    
    // Format response object to a more consistent object
    .then((data) => {
        if (Array.isArray(data.products) && data.products.length) return data;
        return { product_count: 0, products: [] };
    })
    // Fetch product html from products array
    .then((data) => {
        
        const products = [];
        let html = '';
        
        
        // Loop through each product id in parallel and store html response in an array of objects with the sort index attached to it
        async.eachOf(data.products, (id, index, done) => {
            utils.api.product.getById(id, { template }, (err, response) => {
                if (err) done(err);

                products.push({ index, html: response });

                done();
            });
        }, (error) => {
            if (error) {
                // show default product listing on an error
                $('#product-listing-container').find(listClass).find('.product').fadeIn();
                $('#product-listing-container .loadingOverlay').hide();
                return;
            }

            // Sort products from ajax requests based on inital sort index
            products
            .sort((a, b) => {
                if (a.index < b.index) return -1;
                else if (a.index > b.index) return 1;
                return 0;
            })
            // Concatenate sorted product html
            .forEach((product) => {
                html += product.html;
            });
            $('#product-listing-container').find(listClass).html(html).find('.product').fadeIn();
             
        });
    });
}
