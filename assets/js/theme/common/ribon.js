import $ from 'jquery';

/* eslint-disable */
export default {
	basicGet: function(url, query) {
		return $.ajax({
			url: url,
			data: query,
			method: 'GET',
		});
	},
	basicPost: function(url, data) {
		return $.ajax({
			url: url,
			data: data,
			method: 'POST'
		});
	},
	get: function(uri) {
		return $.ajax({
			url: window.ribon + '/api/store/' + window.store + uri,
			method: 'GET'
		})
	}
}