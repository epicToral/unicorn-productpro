import { api } from '@bigcommerce/stencil-utils';
import $ from 'jquery';

export default function renderProductColorLength() {
    const $this = $(this);
    api.product.getById($this.data('productid'), { template: 'products/colour-length' }, (err, response) => {
        $this.find('article .card-colour-count').html(response).addClass('card-colour-count--show');
    });
}
