import PageManager from './page-manager';
import $ from 'jquery';
export default class Page extends PageManager {
    onReady() {
        this.movePageHero();
    }

    /**
     * If the first element inserted via the wysiwyg is an image, move it to the page-hero section.
     * *:first-child - Becasue the wysiwig will wrap the <img> in a <p> or <h*> tag
     */
    movePageHero() {
        if ($('.page-hero').length) {
            if ($('.page-content .content > *:first-child > img').length) {
                $('.page-hero').hide();
                $('.page-content .content > *:first-child > img').appendTo('.page-hero');
                $('.page-hero').fadeIn('fast');
            }
        }
    }
}
