import {
    hooks
} from '@bigcommerce/stencil-utils';
import CatalogPage from './catalog';
import $ from 'jquery';
import FacetedSearch from './common/faceted-search';
import sortProducts from './common/product-positioning';
import renderProductColorLength from './common/product-colorcount';
export default class Category extends CatalogPage {
    before() {
        $('#product-listing-container .loadingOverlay').show();
        //done();
    }
    onReady() {
        $('#product-listing-container .loadingOverlay').show();
        $('#product-listing-container .product').fadeOut();
        if ($('#facetedSearch').length > 0) {
            $(".accordion-navigation-actions").hide();
            $(".accordion-content").show();
        }
        const mailChimpVar = typeof this.url.query.goal === 'undefined' && typeof this.url.query.mc_cid === 'undefined' && typeof this.url.query.mc_eid === 'undefined';
        if (mailChimpVar && (window.location.href.indexOf("?") != "-1" && $('#facetedSearch').length > 0 && window.location.href.indexOf("page") == "-1")) {
            this.initFacetedSearch();
        } else {
            this.onSortBySubmit = this.onSortBySubmit.bind(this);
            hooks.on('sortBy-submitted', this.onSortBySubmit);
            this.isFeatured = typeof this.url.query.sort === 'undefined' && this.context.pagination.category.sort === 'featured' || this.url.query.sort === 'featured';
            if (this.isFeatured) {
                sortProducts(this.context);
            } else {
                $('#product-listing-container').find('.product').fadeIn();
            }
            if ($('.category_height').length == 0 || $('.category_height').height() == 0) {
                jQuery(".product-card .card-figcaption").each(function() {
                    var hei = jQuery(this).height();
                    jQuery(this).after("<div class='category_height' style='height:" + hei + "px'>");
                });
            }
        }
        this.changeCategoryDropdownMobile();
    }
    initFacetedSearch() {
        const $productListingContainer = $('#product-listing-container');
        const $facetedSearchContainer = $('#faceted-search-container');
        const $mobileNavContainer = $('#category-mobile-nav-container');
        var productsPerPage;
        if (window.location.href.indexOf("sort") != '-1' && (window.location.href.indexOf("featured") == '-1')) {
            productsPerPage = this.context.categoryProductsPerPage;
        } else {
            productsPerPage = 99;
        }
        const requestOptions = {
            config: {
                category: {
                    shop_by_price: true,
                    products: {
                        limit: productsPerPage,
                    },
                },
            },
            template: {
                productListing: 'category/product-listing',
                sidebar: 'category/sidebar',
                mobileNav: 'category/mobile-nav',
            },
            showMore: 'category/show-more',
        };
        this.facetedSearch = new FacetedSearch(requestOptions, (content) => {
            var perPagePro = this.context.categoryProductsPerPage;
            var chtml = $('"' + content.productListing + '"');
            var category_id = $('#current-category-id').val();
            if (window.location.href.indexOf("sort") != '-1' && (window.location.href.indexOf("featured") == '-1')) {
                $productListingContainer.html(content.productListing);
                $facetedSearchContainer.html(content.sidebar);
                $(".product").css('display', 'block');
                $('html, body').animate({
                    scrollTop: 0,
                }, 100);
                $('#product-listing-container').find('.product').each(function(b) {
                    $(b).show();
                    $(b).css('display', 'block');
                });
            } else {
                var sortedHTML = chtml;
                var sortedHTML = chtml.find('.product').sort(function(a, b) {
                    return (parseInt($(b).attr('prodpro' + category_id + ''))) < (parseInt($(a).attr('prodpro' + category_id + ''))) ? 1 : -1;
                });
                // Show Sorted Products Detail With Each Loop
                var count = [];
                $(".productGrid").empty();
                $(sortedHTML).each(function(i, val) {
                    $(".productGrid").append(val);
                    $(this).show();
                });
                $facetedSearchContainer.html(content.sidebar);
                $(document).foundation('equalizer', 'reflow');
                $('html, body').animate({
                    scrollTop: 0,
                }, 100);
            }
            $(".productGrid").find('.product').each(function(b) {
                $(b).show();
                $(b).css('display', 'block');
            });
        }, {
            context: this.context,
            url: this.url,
        });
    }
    changeCategoryDropdownMobile() {
        const mobileCategorySelect = $('#category-mobile-nav__select');
        mobileCategorySelect.on('change', this.directToCategory);
    }
    directToCategory() {
        const location = window.location;
        const newPath = $(this).val();
        location.href = location.origin + newPath;
    }
}