/* eslint func-names: 0 */
/* eslint no-shadow: 0 */


/*
 Import all product specific js
 */
import { api } from '@bigcommerce/stencil-utils';
import $ from 'jquery';
import 'foundation-sites/js/foundation/foundation';
import 'foundation-sites/js/foundation/foundation.reveal';
import PageManager from './page-manager';
import Review from './product/reviews';
import collapsibleFactory from './common/collapsible';
import ProductDetails from './common/product-details';
import renderProductColorLength from './common/product-colorcount';
import videoGallery from './product/video-gallery';
import { classifyForm } from './common/form-utils';
import modalFactory from './global/modal';
import { breakpointSizes } from './common/media-query-list';
import SlickSlider from './global/slick-slider';
import TextTruncate from './global/text-truncate';

export default class Product extends PageManager {
    constructor(context) {
      $("dt:contains('ProdPro')").filter(function () {
          $(this).hide();
          $(this).next(".productView-info-value").hide();
       }).remove();
        super(context);
        this.url = window.location.href;
        this.$reviewLink = $('[data-reveal-id="modal-review-form"]');
        this.imageModal = modalFactory('#productViewModal')[0];
    }

    onReady() {
        // Listen for foundation modal close events to sanitize URL after review.
        $(document).on('close.fndtn.reveal', () => {
            if (this.url.indexOf('#write_review') !== -1 && typeof window.history.replaceState === 'function') {
                window.history.replaceState(null, document.title, window.location.pathname);
            }
        });

        let validator;

        // Init collapsible
        collapsibleFactory();

        this.productDetails = new ProductDetails($('.productView'), this.context, window.BCData.product_attributes);

        videoGallery();

        const $reviewForm = classifyForm('.writeReview-form');
        const review = new Review($reviewForm);

        $('body').on('click', '[data-reveal-id="modal-review-form"]', () => {
            validator = review.registerValidation(this.context);
        });

        $reviewForm.on('submit', () => {
            if (validator) {
                validator.performCheck();
                return validator.areAll('valid');
            }

            return false;
        });

        this.productReviewHandler();
        this.productExtraInfoHandler();
        this.productDetailsHandler();
        this.productOptionsHandler();
        this.productViewSliderHandler();
        if ($(window).width() > breakpointSizes.medium) {
            this.productViewModalHandler();
        }
        if ($(window).width() < breakpointSizes.medium) {
            this.productViewRelatedHandler();
        }
        // this.productDescriptionHandler();

        $('#product-related-mobileSlider').find('.product').each(renderProductColorLength);
    }

    productReviewHandler() {
        if (this.url.indexOf('#write_review') !== -1) {
            this.$reviewLink.trigger('click');
        }
    }

    productExtraInfoHandler() {
        // If the extra_info_url is defined we make an ajax request to load the
        // page content.
        const $extraInfoUrl = $('#product-extraInfo').data('extra-info-url');
        if ($extraInfoUrl.length) {
            api.getPage(`/${$extraInfoUrl}`, {}, (err, content) => {
                if (content) {
                    const extraInfo = $($.parseHTML(content)).find('.shogun-root');
                    $('#product-extraInfo').append(extraInfo);
                }
            });
        }
    }

    wrapDetailsBox() {
        $(this)
            .addClass('productView-details__toggle')
            .wrapInner('<a href="#"></a>')
            .find('a')
            .append('<svg class="add-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/></svg>')
            .append('<svg class="remove-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20"><path d="M19 13H5v-2h14v2z"/></svg>');
        $(this)
            .nextUntil('h2')
            .wrapAll('<div class="productView-details__box rte" />');
    }

    wrapDetailsBoxOuter() {
        $(this)
            .nextUntil('h2')
            .andSelf()
            .wrapAll('<div class="productView-details__boxouter" />');
    }

    accordianHeadingClick() {
        const $content = $('.productView-details');
        $content.find('.details-open').removeClass('details-open');
        if ($(this).parent().next('.productView-details__box').is(':visible')) {
            $content.find('.productView-details__box').slideUp();
            return false;
        }
        $content.find('.productView-details__box').hide();
        $(this)
            .toggleClass('details-open')
            .parent().next('.productView-details__box').slideToggle();

        // Scroll top of element.
        $([document.documentElement, document.body]).animate({
            scrollTop: $(this).offset().top - 20,
        }, 500);

        return false;
    }

    productDetailsHandler() {
        // Build product description accordian markup.
        const $content = $('.productView-details');
        const $headings = $content.find('h2');

        // Turn each section after an h2 into an accordian element.
        $headings.each(this.wrapDetailsBox);
        $headings.each(this.wrapDetailsBoxOuter);

        $content.find('.productView-details__box').hide();
        $headings.find('a').on('click', this.accordianHeadingClick);
    }

    productDescriptionHandler() {
        const description = $('#productView_description');
        const descriptionMaxHeight = 115;
        if (description.height() > descriptionMaxHeight) {
            description.addClass('textTruncate');
            const truncate = new TextTruncate(description);
            truncate.init();
        }
    }

    productOptionSwatchClick() {
        const title = $(this).find('.form-option-variant').attr('title');
        if (title.length) {
            $(this).parent().find('.form-label small').html(title);
        }
    }

    productOptionSetRectangleClick() {
        const title = $(this).find('.form-option-variant').text();
        if (title.length) {
            $(this).parent().find('.form-label small').html(title);
        }
    }

    productOptionsHandler() {
        const $form = $('.productView-options form');

        $form.find('[data-product-attribute="swatch"] .form-option').on('click', this.productOptionSwatchClick);
        $form.find('[data-product-attribute="set-rectangle"] .form-option').on('click', this.productOptionSetRectangleClick);
    }

    productViewSliderHandler() {
        // Slick slider for mobile.
        const slider = new SlickSlider();
        slider.initSlickSlider($('#productView-mobileSlider'), {
            mobileFirst: true,
            arrows: false,
            // variableWidth: true,
            infinite: false,
            slidesToShow: 1.2,
            slidesToScroll: 1,
            autoplay: false,
            lazyLoad: 'ondemand',
        }, breakpointSizes.small);
    }

    productViewRelatedHandler() {
        // Slick slider for mobile.
        const slider = new SlickSlider();
        slider.initSlickSlider($('#product-related-mobileSlider'), {
            mobileFirst: true,
            arrows: false,
            // variableWidth: true,
            infinite: false,
            slidesToShow: 1.2,
            slidesToScroll: 1,
            autoplay: false,
            lazyLoad: 'ondemand',
            responsive: [
                {
                    breakpoint: 481,
                    settings: {
                        slidesToShow: 2.4,
                        slidesToScroll: 2,
                    },
                },
            ],
        }, breakpointSizes.medium);
    }

    productViewModalHandler() {
        const $modal = $('#productViewModal');
        const $modalHeader = $('.modal-header', $modal);
        const $modalContent = $('.modal-content', $modal);
        const $modalOverlay = $('.loadingOverlay', $modal);
        let timerid = 0;

        // Display modal gallery.
        $('body').on('click', '[data-image-gallery-main]', (event) => {
            event.preventDefault();
            $modalOverlay.show();
            $modal.foundation('reveal', 'open');

            // Simulate click on current thumbnail so it is shown by default.
            const index = $('.productView-thumbnail-link.is-active').parent().index();
            if (index) {
                clearTimeout(timerid);
                timerid = setTimeout(() => {
                    $('.productView-thumbnail', $modal)
                        .eq(index).click()
                        .find('a').addClass('is-active');
                }, 500);
            }
        });

        // Scroll modal gallery to relevant image.
        $modal.on('click', '.productView-thumbnail', (event) => {
            event.preventDefault();
            const $target = $(event.currentTarget);
            const $currentImage = $('.productView-modal-image', $modalContent).eq($target.index());
            $('.is-active').removeClass('is-active');
            $target.find('a').addClass('is-active');
            $modal.animate({
                scrollTop: ($currentImage.position().top) - $modalHeader.height(),
            }, 500);
        });

        $modalHeader.on('click', '.modal-close', () => {
            // Pass current active state from modal back to main page thumbnails.
            const index = $('.productView-thumbnail-link.is-active').parent().index();
            $('[data-image-gallery]').find('.productView-thumbnail-link').eq(index).click().addClass('is-active');
        });
    }
}
