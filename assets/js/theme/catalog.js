import PageManager from './page-manager';
import $ from 'jquery';
import urlUtils from './common/url-utils';
import Url from 'url';

export default class CatalogPage extends PageManager {
    constructor(props) {
        super(props);

        this.url = Url.parse(location.href, true);
    }

    onSortBySubmit(event) {
        this.url = Url.parse(location.href, true);
        const queryParams = $(event.currentTarget).serialize().split('=');

        this.url.query[queryParams[0]] = queryParams[1];
        delete this.url.query.page;

        event.preventDefault();

        window.location = Url.format({ pathname: this.url.pathname, search: urlUtils.buildQueryString(this.url.query) });
    }
}
