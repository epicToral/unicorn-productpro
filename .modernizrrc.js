"use strict";

module.exports = {
  options: [
    "setClasses"
  ],
  "feature-detects": [
    "css/flexbox",
    "css/objectfit"
  ]
};